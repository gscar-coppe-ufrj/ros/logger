/*!
 * \file logger.h
 */
 


#ifndef LOGGER_LOGGER_H
#define LOGGER_LOGGER_H



#include <string>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include <logger/log_agent.h>
#include <logger/macros.h>



namespace logger {



/*!
 * \class Logger
 * \brief The Logger class is the base class to use the logging system.
 */
class Logger
{
protected:
    /// The internal log agent used by the macros.
    LogAgent* logSystem;
    
public:
    /*!
     * \brief Logger does nothing.
     */
    Logger() {};
    /*!
     * \brief ~Logger does nothing.
     */
    virtual ~Logger() {};
};



}



#endif

