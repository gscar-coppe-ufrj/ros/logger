/*!
 * \file macros.h
 */
 
 
 
#ifndef LOGGER_MACROS_H
#define LOGGER_MACROS_H

#define USE_ROS_LOGGING

#ifdef USE_ROS_LOGGING
    #include <ros/console.h>
#endif

/*
 * This is copied directly from ros::console.h
 * http://docs.ros.org/jade/api/rosconsole/html/console_8h_source.html
 */
#if defined(MSVC)
    #define __LOGGER_FUNCTION__ __FUNCSIG__
#elif defined(__GNUC__)
    #define __LOGGER_FUNCTION__ __PRETTY_FUNCTION__
#else
    #define __LOGGER_FUNCTION__ ""
#endif

#define LOGGER_DEBUG(args) \
    LOGGER_LOG(logger::LogLevel::Debug, args)

#define LOGGER_INFO(args) \
    LOGGER_LOG(logger::LogLevel::Info, args)

#define LOGGER_WARN(args) \
    LOGGER_LOG(logger::LogLevel::Warn, args)

#define LOGGER_ERROR(args) \
    LOGGER_LOG(logger::LogLevel::Error, args)

#define LOGGER_FATAL(args) \
    LOGGER_LOG(logger::LogLevel::Fatal, args)


#define LOGGER_LOG(level, args) \
    do \
    { \
        if (logSystem == NULL) break; \
        std::stringstream __logger_print_stream__ss__; \
        __logger_print_stream__ss__ << args; \
        logSystem->Log(level, __logger_print_stream__ss__, __FILE__, __LINE__, __LOGGER_FUNCTION__); \
    } while (0)



namespace logger {



#ifdef USE_ROS_LOGGING
enum LogLevel {
    Debug       = ::ros::console::levels::Debug,
    Info        = ::ros::console::levels::Info,
    Warn        = ::ros::console::levels::Warn,
    Error       = ::ros::console::levels::Error,
    Fatal       = ::ros::console::levels::Fatal,

    ROS_MAX     = ::ros::console::levels::Count
};
#else
enum LogLevel {
    Debug       = 0,
    Info        = 1,
    Warn        = 2,
    Error       = 3,
    Fatal       = 4,

    ROS_MAX     = 5
};
#endif



}



#endif

