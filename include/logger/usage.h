/*!
 * \mainpage Logger documentation
 *
 * \section usage_sec Usage
 * The class that wants to use the logging system, must be derived from 
 * logger::Logger.
 * 
 * At the constructor you should create a new object using the type of logger
 * you want, for example:
 *
 * \code
 * logSystem = new logger::RosLogger;
 * \endcode
 * 
 * Afterwards, you can configure the node name to be used while logging. This 
 * step is optional. In the case of RosLogger you can use the following code to 
 * get the node name: 
 *
 * \code
 * logSystem->SetNodeName(privateNH.getNamespace());
 * \endcode
 *
 * \section log_types Log types
 * The class provides macros for five levels of priority:
 *
 * \code
 * LOGGER_DEBUG("Testing debug.");
 * LOGGER_INFO("Testing info.");
 * LOGGER_WARN("Testing warning.");
 * LOGGER_ERROR("Testing error.");
 * LOGGER_FATAL("Testing fatal.");
 * \endcode
 *
 * \li Debug should be used for computation methods. For example, if your code
 * computes something using a loop, you should use debug, as it is filtered by 
 * default, so the log is not flooded while the program is not being debugged.
 * \li Info should be used to inform the user about something expected that 
 * occurred with the program. Something like 'Module X loaded'.
 * \li Warning should be used when something not expected occours but the 
 * program can continue executing normally. Something like 'Unable to load 
 * config file X. Creating new config file_path'.
 * \li Error should be used to indicate that something occurred and the class 
 * cannot continue executing. Something like 'Unable to load camera at 
 * /dev/video0'.
 * \li Fatal should be used when something occurred and the entire program 
 * cannot continue executing. Something like 'Qt: Unable to create window'.
 *
 */
