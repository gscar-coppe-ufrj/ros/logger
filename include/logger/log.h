// log.h



#ifndef LOGGER_LOG_H
#define LOGGER_LOG_H


#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <vector>
#include <chrono>
#include <string>
#include <iostream>
#include <fstream>



class TimingLog {
protected:
    typedef boost::posix_time::ptime Time;
    typedef boost::posix_time::time_duration TimeDuration;

    typedef std::chrono::high_resolution_clock Clock;

    boost::mutex mut;
    std::vector<std::string> data_vec;
    std::vector<std::string> name;
    std::string path;
    Time start_time;

    Time t1;
    Time t2;
    bool counting;
    bool recording;

public:

    /*!
     * \brief Class constructor
     * \param path_from_home Full log file path starting from the user's home directory
     */
    TimingLog(std::string path_from_home);

    /*!
     * \brief Sets the full log file path starting from the user's home directory
     * \param path_from_home Full log file path starting from the user's home directory
     */
    void setFileName(std::string path_from_home);

    void startTimer();
    void endTimer(std::string comment);
    std::string getTime();
    std::string getTimeChrono();

    void addToLog(std::string data, std::string comment = "");
    void setRecording(bool val);
    void Save();

    ~TimingLog();
};



#endif // LOGGER_LOG_H
