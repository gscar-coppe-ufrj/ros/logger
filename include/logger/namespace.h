/*!
 * \file namespace.h
 */



#ifndef NAMESPACE_H
#define NAMESPACE_H



/*!
 * \namespace logger
 * \brief The logger namespace provides a classes and macros to easily send log
 *      messages through ROS, stdout or any other method.
 */
namespace logger {
}



#endif

