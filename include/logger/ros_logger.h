/*!
 * \file ros_logger.h
 */
 
 
 
#ifndef LOGGER_LOGGER_ROS_H
#define LOGGER_LOGGER_ROS_H


#include <logger/logger.h>
#include <ros/ros.h>
#include <ros/console.h>



namespace logger {



/*!
 * \class RosLogger
 * \brief The RosLogger class is a log agent using ROS to provide the log
 *      visualization layer.
 */
class RosLogger : public LogAgent
{
    /// ROS node handle.
    ros::NodeHandle nodeHandle;

    /// ROS publisher.
    ros::Publisher pub;
public:
    /*!
     * \brief RosLogger does nothing.
     */
    RosLogger();
    
    /*!
     * \brief Log is called by the logger macros to submit a log message.
     * \param level The verbosity level of the log.
     * \param ss The message to be logged.
     * \param file The file where the function is called (this is set by the macros).
     * \param line The line where the function is called (this is set by the macros).
     * \param function The name of the function where it is called (this is set by the macros).
     */
    void Log(logger::LogLevel level, const std::stringstream& ss, const char* file, int line,
            const char* function);
    
    /*!
     * \brief ~RosLogger does nothing.
     */
    virtual ~RosLogger();
};



}



#endif
