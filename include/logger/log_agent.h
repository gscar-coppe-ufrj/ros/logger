/*!
 * \file logger.h
 */
 
 
 
#ifndef LOGGER_LOGAGENT_H
#define LOGGER_LOGAGENT_H



#include <string>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include <logger/macros.h>



namespace logger {



/*!
 * \class LogAgent
 * \brief The LogAgent class is the base class for log interfaces.
 */
class LogAgent
{
    /// Name of the node.
    std::string nodeName;

    /// The lower priority level to log.
    LogLevel logLevel;
public:
    /*!
     * \brief LogAgent does nothing.
     */
    LogAgent();
    
    /*!
     * \brief Log is called by the logger macros to submit a log message.
     * \param level The verbosity level of the log.
     * \param ss The message to be logged.
     * \param file The file where the function is called (this is set by the macros).
     * \param line The line where the function is called (this is set by the macros).
     * \param function The name of the function where it is called (this is set by the macros).
     */
    virtual void Log(LogLevel level, const std::stringstream& ss, 
            const char* file, int line, const char* function) = 0;

    /*!
     * \brief SetNodeName sets the node name used when making the log message.
     * \param name The new name to be used.
     */
    void SetNodeName(const std::string& name);

    /*!
     * \brief GetNodeName returns the internal node name used by the LogAgent.
     * \return The node name used by the LogAgent.
     */
    const std::string GetNodeName() const;

    /*!
     * \brief SetLoggerLevel sets the minimum log level to send when calling
     *      LogAgent::Log().
     * \param level The log level to send.
     */
    void SetLoggerLevel(LogLevel level);

    /*!
     * \brief GetLoggerLevel gets the minimum log level this class is using.
     * \return LogAgent::logLevel.
     */
    LogLevel GetLoggerLevel();

    /*!
     * \brief ~LogAgent does nothing.
     */
    virtual ~LogAgent();
};



}



#endif

