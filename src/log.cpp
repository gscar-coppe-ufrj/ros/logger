#include <logger/log.h>

#define CSV

TimingLog::TimingLog(std::string path_from_home)
{
    start_time = boost::posix_time::microsec_clock::local_time();
    path = path_from_home;
    counting = false;
    recording = false;
}

void TimingLog::setFileName(std::string path_from_home)
{
    path = path_from_home;
}

void TimingLog::startTimer()
{
    if (!counting && recording)
    {   
        t1 = boost::posix_time::microsec_clock::local_time();
        counting = true;
    }
}

void TimingLog::endTimer(std::string comment)
{
    if (counting && recording)
    {
        t2 = boost::posix_time::microsec_clock::local_time();
        TimeDuration td = t2- t1;

        std::stringstream ss;
        ss << td;
        std::cout << comment << " :" << ss.str() << std::endl;
        counting = false;
    }
}

std::string TimingLog::getTime()
{
    return boost::posix_time::to_simple_string( boost::posix_time::microsec_clock::local_time() );
}

std::string TimingLog::getTimeChrono()
{
    return std::to_string(
                std::chrono::duration_cast<std::chrono::microseconds>(
                    std::chrono::steady_clock::now().time_since_epoch()
                ).count());
}

void TimingLog::addToLog(std::string data, std::string comment)
{
    mut.lock();
    if (recording)
    {
#ifdef CSV
        std::string d = getTimeChrono() + "," + data;
#else
        std::string d = "[" + getTimeChrono() + "] " + data;
#endif
        data_vec.push_back(d);
        name.push_back(comment);
    }
    mut.unlock();
}

void TimingLog::setRecording(bool val)
{
    mut.lock();
    if (recording == true && val == false)
        Save();
    recording = val;
    mut.unlock();
}

void TimingLog::Save()
{
    start_time = boost::posix_time::microsec_clock::local_time();
    int i = 0;
    int size = name.size();
    std::vector<std::string> temp;
    std::string dir_path = getenv("HOME") + path + boost::posix_time::to_iso_string(start_time);
    boost::filesystem::path dir(dir_path);
    boost::filesystem::create_directories(dir);
    // Separates variables by names
    while(i < size)
    {
        if (std::find(temp.begin(), temp.end(), name[i]) == temp.end())
        {
            std::ofstream logfile;
#ifdef CSV
            logfile.open( dir_path + "/" + name[i] + ".csv");
#else
            logfile.open( dir_path + "/" + name[i] + ".log");
#endif
            logfile << data_vec[i] << std::endl;
            temp.push_back(name[i]);
            for (int j = i+1; j < size;  j++)
            {
                if (name[i] == name[j])
                    logfile << data_vec[j] << std::endl;

            }
            logfile.close();
        }
        i++;
    }
    data_vec.clear();
    name.clear();
}

TimingLog::~TimingLog()
{
    if (recording)
        Save();
}
