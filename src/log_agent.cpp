// logger_ros.cpp



#include <logger/log_agent.h>



using namespace logger;
using namespace std;



LogAgent::LogAgent()
{
    SetLoggerLevel(LogLevel::Info);
}


void LogAgent::SetNodeName(const string& name)
{
    nodeName = name;
}


const string LogAgent::GetNodeName() const
{
    return nodeName;
}


void LogAgent::SetLoggerLevel(LogLevel level)
{
    logLevel = level;
}


LogLevel LogAgent::GetLoggerLevel()
{
    return logLevel;
}


LogAgent::~LogAgent()
{
}

