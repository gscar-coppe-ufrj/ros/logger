// logger_ros.cpp



#include <logger/ros_logger.h>
#include <rosgraph_msgs/Log.h>
#include <nodelet/nodelet.h>



using namespace std;
using namespace logger;
using namespace ros;



const char* COLOR_NORMAL    = "\033[0m";
const char* COLOR_RED       = "\033[31m";
const char* COLOR_GREEN     = "\033[32m";
const char* COLOR_GREY          = "\e[37m";
const char* COLOR_YELLOW    = "\033[33m";



RosLogger::RosLogger()
{
    SetNodeName(this_node::getName());
    pub = nodeHandle.advertise<rosgraph_msgs::Log>("/rosout", 1);
}


void RosLogger::Log(LogLevel level, const std::stringstream& ss,
                    const char* file, int line, const char* function)
{
    if (level < GetLoggerLevel())
        return;

    rosgraph_msgs::LogPtr msg(new rosgraph_msgs::Log);
    
    msg->header.stamp = ros::Time::now();

    switch ((int)level)
    {
    case LogLevel::Debug:
        msg->level = rosgraph_msgs::Log::DEBUG;
        break;
    case LogLevel::Info:
        msg->level = rosgraph_msgs::Log::INFO;
        break;
    case LogLevel::Warn:
        msg->level = rosgraph_msgs::Log::WARN;
        break;
    case LogLevel::Error:
        msg->level = rosgraph_msgs::Log::ERROR;
        break;
    case LogLevel::Fatal:
        msg->level = rosgraph_msgs::Log::FATAL;
        break;
    default:
        msg->level = rosgraph_msgs::Log::DEBUG;
        break;
    }

    msg->name = GetNodeName();
    msg->msg = ss.str();
    
    msg->file = file;
    msg->function = function;
    msg->line = line;
    
    this_node::getAdvertisedTopics(msg->topics);
    
    pub.publish(msg);
    
    // Print message to screen
    stringstream sl;
    
    switch ((int)level)
    {
    case LogLevel::Debug:
        sl << COLOR_GREEN << "[DEBUG] [" << msg->header.stamp.sec << "." <<
                msg->header.stamp.nsec << "]: " <<  ss.str() << COLOR_NORMAL;
        cout << sl.str() << endl << flush;
        break;
    case LogLevel::Info:
        sl << COLOR_GREY << "[ INFO] [" << msg->header.stamp.sec << "." <<
                msg->header.stamp.nsec << "]: " <<  ss.str() << COLOR_NORMAL;
        cout << sl.str() << endl << flush;
        break;
    case LogLevel::Warn:
        sl << COLOR_YELLOW << "[ WARN] [" << msg->header.stamp.sec << "." <<
                msg->header.stamp.nsec << "]: " <<  ss.str() << COLOR_NORMAL;
        cout << sl.str() << endl << flush;
        break;
    case LogLevel::Error:
        sl << COLOR_RED << "[ERROR] [" << msg->header.stamp.sec << "." <<
                msg->header.stamp.nsec << "]: " <<  ss.str() << COLOR_NORMAL;
        cerr << sl.str() << endl << flush;
        break;
    case LogLevel::Fatal:
        sl << COLOR_RED << "[FATAL] [" << msg->header.stamp.sec << "." <<
                msg->header.stamp.nsec << "]: " <<  ss.str() << COLOR_NORMAL;
        cerr << sl.str() << endl << flush;
        break;
    default:
        break;
    }
}


RosLogger::~RosLogger()
{
}

